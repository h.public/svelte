export type AnimationEventType = 'animationstart' | 'animationend' | 'animationiteration';
export type DragEventType =
  | 'dragstart'
  | 'dragend'
  | 'dragenter'
  | 'dragleave'
  | 'dragover'
  | 'drop';
export type FocusEventType = 'focus' | 'blur' | 'focusin' | 'focusout';
export type FormEventType = 'submit' | 'reset';
export type InputEventType = 'input' | 'change';
export type KeyboardEventType = 'keydown' | 'keyup' | 'keypress';
export type MouseEventType = 'mousemove' | 'mousedown' | 'mouseup' | 'click' | 'dblclick';
export type TouchEventType = 'touchstart' | 'touchmove' | 'touchend' | 'touchcancel';
export type TransitionEventType = 'transitionstart' | 'transitionend' | 'transitioncancel';
export type UIEventType = 'scroll';
