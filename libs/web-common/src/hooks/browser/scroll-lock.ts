/**
 * Creates a scroll lock utility that prevents various forms of scrolling and user input.
 *
 * @returns {Object} An object with methods for locking and cleaning up scroll lock.
 */
export const scrollLock = () => {
  /**
   * Prevents default behavior of the wheel event.
   *
   * @param {Event} e - The wheel event.
   */
  const onWheel = (e: Event) => e.preventDefault();

  /**
   * Prevents default behavior of the touchmove event.
   *
   * @param {Event} e - The touchmove event.
   */
  const onTouchMove = (e: Event) => e.preventDefault();

  /**
   * Prevents default behavior of specific keyboard keys.
   *
   * @param {KeyboardEvent} e - The keyboard event.
   */
  const onKeyDown = (e: KeyboardEvent) =>
    ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'].includes(e.key) && e.preventDefault();

  /** Adds event listeners to prevent scrolling and user input. */
  const addEventListeners = () => {
    window.addEventListener('wheel', onWheel, { passive: false });
    window.addEventListener('touchmove', onTouchMove, { passive: false });
    window.addEventListener('keydown', onKeyDown);
    window.addEventListener('beforeunload', cleanUp);
  };

  /** Removes event listeners to restore default scrolling and user input. */
  const removeEventListeners = () => {
    window.removeEventListener('wheel', onWheel);
    window.removeEventListener('touchmove', onTouchMove);
    window.removeEventListener('keydown', onKeyDown);
    window.removeEventListener('beforeunload', cleanUp);
  };

  /**
   * Locks or unlocks the scroll and user input.
   *
   * @param {boolean} lock - Whether to enable (true) or disable (false) scroll lock.
   */
  const lock = (lock = true) => {
    if (lock) {
      document.body.style.overflow = 'hidden';
      addEventListeners();
    } else {
      document.body.style.overflow = 'visible';
      removeEventListeners();
    }
  };

  /** Cleans up event listeners before the window unloads. */
  const cleanUp = () => {
    removeEventListeners();
    window.removeEventListener('beforeunload', cleanUp);
  };

  // Initialize by adding the beforeunload event listener
  window.addEventListener('beforeunload', cleanUp);

  return {
    /**
     * Locks or unlocks the scroll and user input.
     *
     * @example
     *   // Enable scroll lock
     *   scrollLock.lock(true);
     *
     *   // Disable scroll lock
     *   scrollLock.lock(false);
     *
     * @param {boolean} lock - Whether to enable (true) or disable (false) scroll lock.
     */
    lock,

    /**
     * Cleans up event listeners before the window unloads.
     *
     * @example
     *   // Clean up event listeners manually
     *   scrollLock.cleanUp();
     */
    cleanUp,
  };
};
