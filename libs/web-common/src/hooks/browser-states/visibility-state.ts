import { createSubscriber } from '@common/events/subscriber.js';

/**
 * Creates a visibility state tracker that allows subscribing to visibility state changes.
 *
 * @returns {Object} The visibility state tracker object.
 */
export const visibilityState = () => {
  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<boolean>();

  let state = document.visibilityState;

  /** Event handler for visibility state changes. */
  const onChange = () => {
    if (document.visibilityState === state) return;
    state = document.visibilityState;
    notifySubscribers(isVisible());
  };

  /**
   * Checks if the document is currently visible.
   *
   * @example
   *   const currentlyVisible = visibilityTracker.isVisible();
   *
   * @returns {boolean} Indicates whether the document is currently visible.
   */
  const isVisible = () => state === 'visible';

  /**
   * Cleans up the event listener.
   *
   * @example
   *   visibilityTracker.cleanUp();
   */
  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);
    window.removeEventListener('visibilitychange', onChange);
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);
  window.addEventListener('visibilitychange', onChange);

  return {
    isVisible,
    subscribe,
    unsubscribe,
    cleanUp,
  };
};
