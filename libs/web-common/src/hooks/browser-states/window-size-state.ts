import { createSubscriber } from '@common/events/subscriber.js';

/**
 * Creates a window size state tracker that allows subscribing to window size changes.
 *
 * @param {string} [mediaQueries] - Media queries to track window size changes based on specific
 *   conditions. This parameter is optional.
 * @returns {Object} The window size state tracker object.
 */
export const windowSizeState = (mediaQueries?: string) => {
  const query = mediaQueries && matchMedia(mediaQueries);

  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<Size>();

  const getSize = () => {
    return {
      width: window.innerWidth,
      height: window.innerHeight,
    };
  };

  /** Event handler for window resize. */
  const onResize = () => {
    notifySubscribers({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);

    if (query) {
      query.removeEventListener('change', onResize);
    } else {
      window.removeEventListener('resize', onResize);
    }
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);

  if (query) {
    query.addEventListener('change', onResize);
  } else {
    window.addEventListener('resize', onResize);
  }

  return {
    /**
     * Retrieves the current window size.
     *
     * @example
     *   const size = windowSizeTracker.getSize();
     *   console.log(size); // { width: 1024, height: 768 }
     *
     * @returns {Size} The current window size object with width and height properties.
     */
    getSize,
    /**
     * Subscribes to window size changes.
     *
     * @param {Function} callback - The callback function to be called when the window size changes.
     * @returns {void}
     */
    subscribe,
    /**
     * Unsubscribes from window size changes.
     *
     * @param {Function} callback - The callback function to be unsubscribed.
     * @returns {void}
     */
    unsubscribe,
    /**
     * Cleans up the event listeners and the state tracker.
     *
     * @example
     *   windowSizeTracker.cleanUp();
     */
    cleanUp,
  };
};

export type Size = {
  width: number;
  height: number;
};
