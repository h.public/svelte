import { createSubscriber } from '@common/events/subscriber.js';

/**
 * Creates a focus state tracker that allows subscribing to focus state changes.
 *
 * @returns {Object} The focus state tracker object.
 */
export const focusState = () => {
  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<boolean>();

  let state = !document.hidden;

  /** Event handler for when the window gains focus. */
  const onFocus = () => {
    if (state) return;
    state = true;
    notifySubscribers(state);
  };

  /** Event handler for when the window loses focus. */
  const onBlur = () => {
    if (!state) return;
    state = false;
    notifySubscribers(state);
  };

  /**
   * Gets the current focus state.
   *
   * @example
   *   const isCurrentlyFocused = focusTracker.isFocus();
   *
   * @returns {boolean} The current focus state.
   */
  const isFocus = () => state;

  /**
   * Cleans up the event listeners.
   *
   * @example
   *   focusTracker.cleanUp();
   */
  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);
    window.removeEventListener('focus', onFocus);
    window.removeEventListener('blur', onBlur);
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);
  window.addEventListener('focus', onFocus);
  window.addEventListener('blur', onBlur);

  return {
    isFocus,
    subscribe,
    unsubscribe,
    cleanUp,
  };
};
