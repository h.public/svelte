import { createSubscriber } from '@common/events/subscriber.js';

/**
 * Creates a key input event tracker.
 *
 * @example
 *   const keyInputTracker = inputKeyTracker();
 *   keyInputTracker.subscribe((eventInfo) => {
 *     console.log('Key:', eventInfo.key);
 *     console.log('Keys pressed:', eventInfo.keysPressed);
 *     console.log('Event type:', eventInfo.type);
 *   });
 *   keyInputTracker.cleanUp(); // Call this to clean up event listeners and clear subscribers when done.
 *
 *   // Alternatively, listen for key events on a specific element:
 *   const targetElement = document.getElementById('myElement');
 *   const keyInputTrackerOnElement = inputKeyTracker(targetElement);
 *   keyInputTrackerOnElement.subscribe((eventInfo) => {
 *     // Handle key events on the target element.
 *   });
 *   keyInputTrackerOnElement.cleanUp();
 *
 * @param {HTMLElement | Document} [target=document] - The target element to listen for key events.
 *   Default is `document`
 * @returns {KeyInputTracker} The key input event tracker object.
 */
export const inputKeyTracker = (target: HTMLElement | Document = document) => {
  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<KeyInputInfo>();
  const keysPressed: Record<string, boolean> = {};

  const onKeyDown = (e: KeyboardEvent) => {
    const key = e.key;
    keysPressed[key] = true;

    notifySubscribers({
      keyDown: key,
      keysPressed,
    });
  };

  const onKeyUp = (e: KeyboardEvent) => {
    const key = e.key;
    keysPressed[key] = false;

    notifySubscribers({
      keyUp: key,
      keysPressed,
    });
  };

  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);
    target.removeEventListener('keydown', onKeyDown as EventListenerOrEventListenerObject);
    target.removeEventListener('keyup', onKeyUp as EventListenerOrEventListenerObject);
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);
  target.addEventListener('keydown', onKeyDown as EventListenerOrEventListenerObject);
  target.addEventListener('keyup', onKeyUp as EventListenerOrEventListenerObject);

  return {
    /** Subscribes to the touch or mouse events. */
    subscribe,
    /** Unsubscribes from the touch or mouse events. */
    unsubscribe,
    /** Cleans up event listeners and clears subscribers. */
    cleanUp,
  };
};

export type KeyInputInfo = {
  keyDown?: string;
  keyUp?: string;
  keysPressed: Record<string, boolean>;
};
