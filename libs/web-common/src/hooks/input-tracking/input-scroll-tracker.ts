import { createSubscriber } from '@common/events/subscriber.js';

/**
 * Creates a scroll tracking system that allows components to subscribe to scroll events.
 *
 * @returns {{
 *   subscribe: (callback: (info: InputScrollInfo) => void) => void;
 *   unsubscribe: () => void;
 *   cleanUp: () => void;
 * }}
 *   The scroll tracking system with subscription methods.
 */
export const inputScrollTracker = () => {
  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<InputScrollInfo>();

  let prevScrollPos = document.documentElement.scrollTop;

  /** Handles the scroll event and notifies subscribers. */
  const onScroll = () => {
    const currentScrollPos = document.documentElement.scrollTop;
    notifySubscribers({
      scrollTop: document.documentElement.scrollTop,
      direction: currentScrollPos > prevScrollPos ? 'down' : 'up',
    });

    prevScrollPos = currentScrollPos;
  };

  /** Cleans up event listeners and clears subscribers when the page is unloaded. */
  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);
    document.removeEventListener('scroll', onScroll);
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);
  document.addEventListener('scroll', onScroll);

  return {
    /** Subscribes to the touch or mouse events. */
    subscribe,
    /** Unsubscribes from the touch or mouse events. */
    unsubscribe,
    /** Cleans up event listeners and clears subscribers. */
    cleanUp,
  };
};

export type InputScrollInfo = {
  scrollTop: number;
  direction: 'up' | 'down';
};
