import { createSubscriber } from '@common/events/subscriber.js';
import type { DragEventType } from '@web-common/types/events.js';

/**
 * Tracks drag events on a specified element.
 *
 * @param {Element} element - The element on which to track drag events.
 * @param {Partial<DragEventType[]>} [types=['dragenter', 'dragleave', 'drop']] - The drag event
 *   types to track. Default is `['dragenter', 'dragleave', 'drop']`
 * @returns {Object} An object with subscribe, unsubscribe, and cleanUp methods.
 */
export const dragEventTracker = (
  element: Element,
  types: Partial<DragEventType[]> = ['dragenter', 'dragleave', 'drop'],
) => {
  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<DragEvent>();
  const target = element;

  const eventHandler = (event: Event) => {
    const e = event as DragEvent;
    notifySubscribers(e);
  };

  /** Cleans up event listeners and clears subscribers. */
  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);
    types.forEach((type) => {
      target?.removeEventListener(type as keyof ElementEventMap, eventHandler);
    });
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);
  types.forEach((type) => {
    target.addEventListener(type as keyof ElementEventMap, eventHandler);
  });
  return {
    /** Subscribe to drag events. */
    subscribe,
    /** Unsubscribe a subscriber from drag events. */
    unsubscribe,
    /** Clean up event listeners and clear all subscribers. */
    cleanUp,
  };
};
