import { createSubscriber } from '@common/events/subscriber.js';
import { isMobile } from '@web-common/helpers/DeviceHelper.js';

/**
 * Tracks movement information based on mouse or touch events.
 *
 * @returns {Object} An object containing the subscribe, unsubscribe, and cleanUp methods.
 */
export const inputMovementTracker = () => {
  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<MovementInfo>();
  const moveType = isMobile() ? 'touchmove' : 'mousemove';

  let prevX: number, prevY: number, prevSpeed: number;
  let currX: number, currY: number;
  let move: number, moveX: number, moveY: number;
  let speed: number, acceleration: number;

  const onMove = (e: TouchEvent | MouseEvent) => {
    if (e instanceof TouchEvent) {
      currX = e.targetTouches[0].clientX;
      currY = e.targetTouches[0].clientY;
    } else {
      currX = e.clientX;
      currY = e.clientY;
    }

    if (prevX && currX) {
      moveX = Math.round(currX - prevX);
      moveY = Math.round(currY - prevY);
      move = Math.round(Math.sqrt(moveX * moveX + moveY * moveY));

      speed = Math.round(10 * move); //current speed
      acceleration = Math.round(10 * (speed - prevSpeed));
    }

    prevX = currX;
    prevY = currY;
    prevSpeed = speed;

    notifySubscribers({
      move,
      moveX,
      moveY,
      speed,
      acceleration,
    });
  };

  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);
    window.removeEventListener(moveType, onMove);
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);
  window.addEventListener(moveType, onMove, { passive: true });

  return {
    /** Subscribes to the touch or mouse events. */
    subscribe,
    /** Unsubscribes from the touch or mouse events. */
    unsubscribe,
    /** Cleans up event listeners and clears subscribers. */
    cleanUp,
  };
};

export type MovementInfo = {
  move: number;
  moveX: number;
  moveY: number;
  speed: number;
  acceleration: number;
};
