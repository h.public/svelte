import { createSubscriber } from '@common/events/subscriber.js';
import { isMobile } from '@web-common/helpers/DeviceHelper.js';
import type { MouseEventType, TouchEventType } from '@web-common/types/events.js';

/**
 * Creates an input click tracker to subscribe to touch or mouse events on a specified element or
 * the window object.
 *
 * @template T - The event type, either TouchEvent or MouseEvent.
 * @param {Element | null | undefined} [element] - The element to attach the event listeners to. If
 *   not provided, the window object will be used.
 * @param {Partial<TouchEventType[] | MouseEventType[]>} [types=[]] - The array of touch or mouse
 *   event types to subscribe to. If not provided, it falls back to the default events based on the
 *   device type. Default is `[]`
 * @returns {Object} - An object containing the subscribe, unsubscribe, and cleanUp functions.
 */
export const inputClickTracker = <T extends TouchEvent | MouseEvent>(
  element?: Nullable<Element>,
  types: Partial<TouchEventType[] | MouseEventType[]> = [],
) => {
  const { subscribe, unsubscribe, notifySubscribers, clear } = createSubscriber<T>();
  const target = element || window;

  let eventTypes = [...types];
  if (!eventTypes.length) {
    const bool = isMobile();
    const defaultEvents: (TouchEventType | MouseEventType)[] = bool
      ? ['touchstart', 'touchend']
      : ['mousedown', 'mouseup'];
    eventTypes.push(...defaultEvents);
  }

  const eventHandler = (event: Event) => {
    const e = event as T;
    notifySubscribers(e);
  };

  /** Cleans up event listeners and clears subscribers. */
  const cleanUp = () => {
    window.removeEventListener('beforeunload', cleanUp);
    eventTypes.forEach((type) => {
      target.removeEventListener(type as keyof ElementEventMap, eventHandler);
    });
    clear();
  };

  window.addEventListener('beforeunload', cleanUp);
  eventTypes.forEach((type) => {
    target.addEventListener(type as keyof ElementEventMap, eventHandler, { passive: true });
  });

  return {
    /** Subscribes to the touch or mouse events. */
    subscribe,
    /** Unsubscribes from the touch or mouse events. */
    unsubscribe,
    /** Cleans up event listeners and clears subscribers. */
    cleanUp,
  };
};
