import { isPortrait } from './DeviceHelper.js';

/**
 * Determines the best platform (mobile, tablet, or PC) based on the screen size and orientation.
 *
 * @param {Object} config - Configuration object with screen size thresholds for mobile and tablet.
 * @returns {ResponsivePlatform} Returns the best platform as an enum value (MOBILE, TABLET, or PC).
 */
export const determineBestPlatform = (config = ScreenSizeConfig) => {
  const portrait = isPortrait();
  const screenWidth: number = window.innerWidth;
  const screenHeight: number = window.innerHeight;
  const aspectRatio = screenWidth / screenHeight;

  if (portrait) {
    if (screenHeight < config.tablet || aspectRatio < 0.6) {
      return ResponsivePlatform.MOBILE;
    }

    if (screenHeight < config.pc || aspectRatio < 1.5) {
      return ResponsivePlatform.TABLET;
    }
  } else {
    if (screenWidth < config.tablet || aspectRatio < 0.6) {
      return ResponsivePlatform.MOBILE;
    }

    if (screenWidth < config.pc || aspectRatio < 1.5) {
      return ResponsivePlatform.TABLET;
    }
  }

  return ResponsivePlatform.PC;
};

/**
 * Determines if the current view is a mobile view based on the given configuration.
 *
 * @param {ScreenSizeConfig} [config=ScreenSizeConfig] - The configuration for screen sizes. Default
 *   is `ScreenSizeConfig`. Default is `ScreenSizeConfig`
 * @returns {boolean} `true` if the current view is a mobile view, otherwise `false`.
 */
export const isMobileView = (config = ScreenSizeConfig) =>
  determineBestPlatform(config) === ResponsivePlatform.MOBILE;

export const ScreenSizeConfig = {
  tablet: 768,
  pc: 1024,
};

export const enum ResponsivePlatform {
  MOBILE,
  TABLET,
  PC,
}
