/**
 * Returns the operating system of the device.
 *
 * @returns {OsEnum} The operating system of the device.
 * @throws {Error} If the operating system of the device cannot be determined.
 */
export const getOs = () => {
  const userAgent: string = navigator.userAgent;
  const macosPlatforms: string[] = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K', 'Mac OS X'];
  const windowsPlatforms: string[] = ['Win32', 'Win64', 'Windows', 'WinCE'];
  const iosPlatforms: string[] = ['iPhone', 'iPad', 'iPod'];

  if (macosPlatforms.some((platform) => userAgent.includes(platform))) {
    return OsEnum.MacOs;
  }

  if (iosPlatforms.some((platform) => userAgent.includes(platform))) {
    return OsEnum.iOS;
  }

  if (windowsPlatforms.some((platform) => userAgent.includes(platform))) {
    return OsEnum.Windows;
  }

  if (/Android/.test(userAgent)) {
    return OsEnum.Android;
  }

  if (/Linux/.test(userAgent)) {
    return OsEnum.Linux;
  }

  throw new Error('Unknown platform');
};

/**
 * Returns whether the device is a mobile device.
 *
 * @returns {boolean} Whether the device is a mobile device.
 */
export const isMobile = () => /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

/**
 * Returns whether the device is a real mobile device (not just a device with a small screen).
 *
 * @returns {boolean} Whether the device is a real mobile device.
 */
export const isRealMobile = () => isMobile() && hasTouchScreen() && navigator.maxTouchPoints > 1;

/**
 * Checks if the user's device has a touch screen.
 *
 * @returns {boolean} True if the user's device has a touch screen, false otherwise.
 */
export const hasTouchScreen = () => {
  const hasTouchScreen = navigator.maxTouchPoints > 0;
  if (hasTouchScreen && matchMedia('(pointer: coarse)').matches) {
    return true;
  }
  return false;
};

/**
 * Returns the version of Android running, if applicable.
 *
 * @returns {number[]} An array containing the version number, or an empty array if the user agent
 *   is not android-based.
 */
export const getAndroidVersion = () => {
  const ua: string = navigator.userAgent;
  const match = ua.match(/Android\s([0-9.]+)/);
  if (match) {
    const versionString = match[1];
    return versionString.split('.').map((n) => parseInt(n));
  }
  return [];
};

/**
 * Get the version number of the iOS device, if applicable.
 *
 * @returns An array containing the version number, or an empty array if the user agent is not
 *   iOS-based.
 */
export const getIOSVersion = () => {
  const ua: string = navigator.userAgent;
  if (/(iPhone|iPod|iPad)/i.test(ua)) {
    const match = ua.match(/OS [\d_]+/i)?.[0];
    return match
      ? match
          .substring(3)
          .split('_')
          .map((n) => parseInt(n))
      : [];
  }
  return [];
};

/**
 * Returns the version of iOS running on the device.
 *
 * @returns {number[]} The version of iOS running on the device.
 */
export const isIOS = () => getOs() === OsEnum.iOS;

/**
 * Returns whether the device is an iOS device.
 *
 * @returns {boolean} Whether the device is an iOS device.
 */
export const isMacOs = () => getOs() === OsEnum.MacOs;

/**
 * Returns whether the device is an Android device.
 *
 * @returns {boolean} Whether the device is an Android device.
 */
export const isAndroid = () => getOs() === OsEnum.Android;

/**
 * Checks whether the device is in portrait orientation.
 *
 * @returns {boolean} Returns true if the device is in portrait orientation, false otherwise.
 */
export const isPortrait = () => window.matchMedia('(orientation: portrait)').matches;

/**
 * A utility class for getting information about the user's device.
 *
 * @namespace DeviceHelper
 */
const DeviceHelper = {
  /** @see {@link getOs} */
  getOs,
  /** @see {@link isMobile} */
  isMobile,
  /** @see {@link isRealMobile} */
  isRealMobile,
  /** @see {@link hasTouchScreen} */
  hasTouchScreen,
  /** @see {@link getAndroidVersion} */
  getAndroidVersion,
  /** @see {@link getIOSVersion} */
  getIOSVersion,
  /** @see {@link isIOS} */
  isIOS,
  /** @see {@link isMacOs} */
  isMacOs,
  /** @see {@link isAndroid} */
  isAndroid,
  /** @see {@link isPortrait} */
  isPortrait,
};

export default DeviceHelper;

/**
 * An enumeration of operating systems.
 *
 * @readonly
 * @enum {string}
 */
const enum OsEnum {
  Windows = 'Windows',
  Android = 'Android',
  iOS = 'iOS',
  MacOs = 'macOS',
  Linux = 'Linux',
}
