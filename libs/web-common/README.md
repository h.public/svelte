# Common TypeScript Library for Web

This is a collection of TypeScript code for common web usage. The library can be added to your project as a Git submodule, allowing you to easily reuse the code across multiple projects.

## Using the Library as a Git Submodule

To add the web-common submodule with the common submodule inside its libs folder, you can follow these steps:

1. Navigate to the root directory of your project and run the following command to add the web-common submodule:

```
git submodule add https://gitlab.com/h.library/web-common.git libs/web-common
```

2. Navigate to the web-common submodule directory:

```
cd libs/web-common
```

3. Add the common submodule to the libs folder of the web-common submodule:

```
git submodule add https://gitlab.com/h.library/common.git libs/common
```

4. Navigate back to the root directory of your project:

```
cd ../..
```

5. Update the web-common and common submodules to the latest versions by running the following command:

```
git submodule update --remote
```

## Configuring the Library in tsconfig

To configure the library in your `tsconfig.json` file, add the following lines:

```
"baseUrl": ".",
"paths": {
  "@common/*": [
    "libs/common/src/*"
  ],
  "@web-common/": [
	  "libs/web-common/src/*"
  ]
}
```
