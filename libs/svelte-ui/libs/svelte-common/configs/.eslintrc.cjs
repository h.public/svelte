/**
 * Resolves the ESLint configuration file path based on the current directory location.
 *
 * @module ESLintConfigResolver
 */

const path = require('path');
const currentDirectory = path.dirname(__filename);
const isUnderLibs = currentDirectory.includes(path.sep + 'libs' + path.sep);
const baseDir = isUnderLibs ? '../../../' : '..';

module.exports = {
  extends: [
    path.resolve(__dirname, baseDir + '/libs/common/configs/.eslintrc'),
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:svelte/recommended',
    'prettier',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020,
    extraFileExtensions: ['.svelte'],
  },
  env: {
    browser: true,
  },
  plugins: ['prettier-plugin-svelte'],
  pluginSearchDirs: ['.'],
  overrides: [
    {
      files: ['*.svelte'],
      parser: 'svelte-eslint-parser',
      parserOptions: {
        parser: '@typescript-eslint/parser',
      },
    },
  ],
};
