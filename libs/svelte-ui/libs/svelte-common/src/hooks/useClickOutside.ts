/**
 * Svelte custom directive for attaching a click outside event listener to the element.
 *
 * @example
 *   <script>
 *   import clickOutside from '@svelte-common/hooks/useClickOutside.js';
 *
 *   function onClickOutside(event) {
 *   console.log('Clicked outside:', event.detail);
 *   }
 *   </script>
 *
 *   <div use:clickOutside={onClickOutside}>
 *   <!-- Your content here -->
 *   </div>
 *
 * @param {HTMLElement} node - The HTML element to which the click outside event listener will be
 *   attached.
 * @param {Function} callback - The callback function to be invoked when a click occurs outside the
 *   element.
 * @returns {{ destroy: () => void }} An object with a `destroy` method that removes the event
 *   listener.
 */
const clickOutside = (node: HTMLElement, callback: Callback) => {
  /**
   * Click event handler to check if a click occurred outside the given node.
   *
   * @param {Event} event - The click event.
   */
  const onClick = (event: Event) => {
    if (node && !node.contains(event.target) && !event.defaultPrevented) {
      callback(event); // Invoke the provided callback function
    }
  };

  document.addEventListener('click', onClick, true);
  return {
    destroy() {
      document.removeEventListener('click', onClick, true);
    },
  };
};

export default clickOutside;
