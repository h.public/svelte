export type ImageInfo = {
  /** The source URL or path of the image */
  src: string;

  /** The width of the image in pixels */
  width: number;

  /** The height of the image in pixels */
  height: number;

  /** The size of the image in bytes */
  size: number;

  /** The file type or format of the image (e.g., "jpeg", "png", etc.) */
  type: string;

  /** The name of the image file */
  name: string;

  /** The timestamp when the image was last modified */
  lastModified: number;
};
