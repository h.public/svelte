/**
 * Modify Link Behavior (No History Recording).
 *
 * This function modifies the default behavior of links (<a> elements). When a link is clicked, it
 * prevents the default navigation behavior and updates the URL hash to indicate the current target
 * without adding an entry to the browser's navigation history.
 *
 * @example
 *   // Usage example:
 *   const slideLinks = document.querySelectorAll('a.slide-link');
 *   slideLinks.forEach((link) => {
 *     link.addEventListener('click', noHistory);
 *   });
 *
 * @param {PointerEvent | Event} event - The click event to handle.
 */
export const noHistory = (event: PointerEvent | Event) => {
  event.preventDefault();

  const link = event.currentTarget as HTMLAnchorElement;
  const href = link.getAttribute('href')!;
  location.replace(href);
};
