import NumberHelper, { Trunc } from '@common/helpers/NumberHelper.js';
import { describe, expect, it } from 'vitest';

describe('NumberHelper', () => {
  describe('trunc', () => {
    it('should truncate the number without decimal places in CLEAN mode', () => {
      expect(NumberHelper.trunc(3.14159, 0, Trunc.CLEAN)).toBe(3);
      expect(NumberHelper.trunc(1234.5678, 0, Trunc.CLEAN)).toBe(1234);
    });

    it('should truncate the number with the specified precision in CLEAN mode', () => {
      expect(NumberHelper.trunc(3.14159, 2, Trunc.CLEAN)).toBe(3.14);
      expect(NumberHelper.trunc(1234.5678, 3, Trunc.CLEAN)).toBe(1234.567);
    });

    it('should round the number with the specified precision in ROUND mode', () => {
      expect(NumberHelper.trunc(3.14159, 2, Trunc.ROUND)).toBe(3.14);
      expect(NumberHelper.trunc(1234.5678, 3, Trunc.ROUND)).toBe(1234.568);
    });

    it('should floor the number with the specified precision in FLOOR mode', () => {
      expect(NumberHelper.trunc(3.14159, 2, Trunc.FLOOR)).toBe(3.14);
      expect(NumberHelper.trunc(1234.5678, 3, Trunc.FLOOR)).toBe(1234.567);
    });
  });

  describe('noLeadingZeroes', () => {
    it('should only allow one leading zero before decimal', () => {
      expect(NumberHelper.noLeadingZeroes(0)).toBe(true);
      expect(NumberHelper.noLeadingZeroes('0')).toBe(true);
      expect(NumberHelper.noLeadingZeroes('00')).toBe(false);
      expect(NumberHelper.noLeadingZeroes(0.01)).toBe(true);
      expect(NumberHelper.noLeadingZeroes('0.01')).toBe(true);
      expect(NumberHelper.noLeadingZeroes(2.12003)).toBe(true);
      expect(NumberHelper.noLeadingZeroes('0.00000')).toBe(true);
      expect(NumberHelper.noLeadingZeroes('0.120000')).toBe(true);
      expect(NumberHelper.noLeadingZeroes(12000000)).toBe(true);
      expect(NumberHelper.noLeadingZeroes('00.12')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('002.12')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('00.12.')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('100.12.')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('00.12.2')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('100.12.2')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('0000.01')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('1,000.0')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('00.120000')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('0.1200.00')).toBe(false);
      expect(NumberHelper.noLeadingZeroes('00000000')).toBe(false);
    });
  });
});
