import DataTypeHelper from '@common/helpers/DataTypeHelper.js';
import { describe, expect, it } from 'vitest';

describe('DataTypeHelper', () => {
  describe('isBoolean', () => {
    it('should return true for true', () => {
      expect(DataTypeHelper.isBoolean(true)).toBe(true);
      expect(DataTypeHelper.isBoolean('true')).toBe(true);
    });

    it('should return true for false', () => {
      expect(DataTypeHelper.isBoolean(false)).toBe(true);
      expect(DataTypeHelper.isBoolean('false')).toBe(true);
    });

    it('should return false for non-boolean values', () => {
      expect(DataTypeHelper.isBoolean(1)).toBe(false);
      expect(DataTypeHelper.isBoolean(null)).toBe(false);
      expect(DataTypeHelper.isBoolean(undefined)).toBe(false);
      expect(DataTypeHelper.isBoolean({})).toBe(false);
      expect(DataTypeHelper.isBoolean([])).toBe(false);
    });
  });

  describe('isNumber', () => {
    it('should return true for valid numbers', () => {
      expect(DataTypeHelper.isNumber(0)).toBe(true);
      expect(DataTypeHelper.isNumber(123)).toBe(true);
      expect(DataTypeHelper.isNumber('123')).toBe(true);
      expect(DataTypeHelper.isNumber('3.14')).toBe(true);
    });

    it('should return false for non-numbers', () => {
      expect(DataTypeHelper.isNumber('abc')).toBe(false);
      expect(DataTypeHelper.isNumber(true as any)).toBe(false);
      expect(DataTypeHelper.isNumber(null as any)).toBe(false);
      expect(DataTypeHelper.isNumber(undefined as any)).toBe(false);
      expect(DataTypeHelper.isNumber({} as any)).toBe(false);
      expect(DataTypeHelper.isNumber([] as any)).toBe(false);
    });
  });

  describe('isString', () => {
    it('should return true for strings', () => {
      expect(DataTypeHelper.isString('hello')).toBe(true);
      expect(DataTypeHelper.isString('123')).toBe(true);
      expect(DataTypeHelper.isString('')).toBe(true);
    });

    it('should return false for non-strings', () => {
      expect(DataTypeHelper.isString(123)).toBe(false);
      expect(DataTypeHelper.isString(true)).toBe(false);
      expect(DataTypeHelper.isString(null)).toBe(false);
      expect(DataTypeHelper.isString(undefined)).toBe(false);
      expect(DataTypeHelper.isString({})).toBe(false);
      expect(DataTypeHelper.isString([])).toBe(false);
    });
  });

  describe('isJson', () => {
    it('should return true for valid JSON strings', () => {
      expect(DataTypeHelper.isJson('{"name":"John","age":30}')).toBe(true);
      expect(DataTypeHelper.isJson('{"name":"John","age":30,"nested":{"prop":true}}')).toBe(true);
    });

    it('should return false for invalid JSON strings', () => {
      expect(DataTypeHelper.isJson('{name:"John",age:30}')).toBe(false);
      expect(DataTypeHelper.isJson('{"name":"John","age":30')).toBe(false);
      expect(DataTypeHelper.isJson(true as any)).toBe(false);
      expect(DataTypeHelper.isJson(null as any)).toBe(false);
      expect(DataTypeHelper.isJson(undefined as any)).toBe(false);
      expect(DataTypeHelper.isJson({} as any)).toBe(false);
      expect(DataTypeHelper.isJson([] as any)).toBe(false);
    });
  });

  describe('isArray', () => {
    it('should return true for arrays', () => {
      expect(DataTypeHelper.isArray([1, 2, 3])).toBe(true);
      expect(DataTypeHelper.isArray([])).toBe(true);
    });

    it('should return false for non-arrays', () => {
      expect(DataTypeHelper.isArray('abc')).toBe(false);
      expect(DataTypeHelper.isArray(true)).toBe(false);
      expect(DataTypeHelper.isArray(null)).toBe(false);
      expect(DataTypeHelper.isArray(undefined)).toBe(false);
      expect(DataTypeHelper.isArray({})).toBe(false);
      expect(DataTypeHelper.isArray(123)).toBe(false);
    });
  });

  describe('isObject', () => {
    it('should return true for objects', () => {
      expect(DataTypeHelper.isObject({ name: 'John', age: 30 })).toBe(true);
      expect(DataTypeHelper.isObject({})).toBe(true);
    });

    it('should return false for non-objects', () => {
      expect(DataTypeHelper.isObject('abc')).toBe(false);
      expect(DataTypeHelper.isObject(true)).toBe(false);
      expect(DataTypeHelper.isObject(null)).toBe(false);
      expect(DataTypeHelper.isObject(undefined)).toBe(false);
      expect(DataTypeHelper.isObject([])).toBe(false);
      expect(DataTypeHelper.isObject(123)).toBe(false);
    });
  });

  describe('isFunction', () => {
    it('should return true for functions', () => {
      expect(
        DataTypeHelper.isFunction(() => {
          /* */
        }),
      ).toBe(true);
      expect(
        DataTypeHelper.isFunction(function () {
          /* */
        }),
      ).toBe(true);
    });

    it('should return false for non-functions', () => {
      expect(DataTypeHelper.isFunction(0)).toBe(false);
      expect(DataTypeHelper.isFunction(1)).toBe(false);
      expect(DataTypeHelper.isFunction('abc')).toBe(false);
      expect(DataTypeHelper.isFunction(true)).toBe(false);
      expect(DataTypeHelper.isFunction(null)).toBe(false);
      expect(DataTypeHelper.isFunction(undefined)).toBe(false);
      expect(DataTypeHelper.isFunction({})).toBe(false);
      expect(DataTypeHelper.isFunction([])).toBe(false);
      expect(DataTypeHelper.isFunction(new Date())).toBe(false);
    });
  });
});
