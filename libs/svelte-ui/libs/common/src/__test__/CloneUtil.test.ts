import CloneUtil from '@common/utils/CloneUtil.js';
import { describe, expect, it } from 'vitest';

describe('CloneUtil', () => {
  describe('clone', () => {
    it('should clone primitive types', () => {
      expect(CloneUtil.clone(42)).toBe(42);
      expect(CloneUtil.clone('Hello')).toBe('Hello');
      expect(CloneUtil.clone(true)).toBe(true);
      expect(CloneUtil.clone(null)).toBe(null);
      expect(CloneUtil.clone(undefined)).toBe(undefined);
    });

    it('should clone an array', () => {
      const array = [1, 2, 3];
      const clonedArray = CloneUtil.clone(array);

      expect(clonedArray).toEqual(array);
      expect(clonedArray).not.toBe(array);
    });

    it('should clone an object', () => {
      const obj = { name: 'John', age: 30 };
      const clonedObj = CloneUtil.clone(obj);

      expect(clonedObj).toEqual(obj);
      expect(clonedObj).not.toBe(obj);
    });

    it('should clone a Map', () => {
      const map = new Map();
      map.set('key1', 'value1');
      map.set('key2', 'value2');

      const clonedMap = CloneUtil.clone(map);

      expect(clonedMap).toEqual(map);
      expect(clonedMap).not.toBe(map);
    });

    it('should clone a Set', () => {
      const set = new Set();
      set.add('value1');
      set.add('value2');

      const clonedSet = CloneUtil.clone(set);

      expect(clonedSet).toEqual(set);
      expect(clonedSet).not.toBe(set);
    });
  });

  describe('cloneObject', () => {
    it('should clone an object', () => {
      const obj = { name: 'John', age: 30 };
      const clonedObj = CloneUtil.cloneObject(obj);

      expect(clonedObj).toEqual(obj);
      expect(clonedObj).not.toBe(obj);
    });

    it('should clone nested objects', () => {
      const obj = {
        name: 'John',
        address: {
          street: '123 Main St',
          city: 'New York',
        },
      };

      const clonedObj = CloneUtil.cloneObject(obj);

      expect(clonedObj).toEqual(obj);
      expect(clonedObj.address).toEqual(obj.address);
      expect(clonedObj.address).not.toBe(obj.address);
    });
  });
});
