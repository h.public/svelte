import MathUtil from '@common/utils/MathUtil.js';
import { describe, expect, it, test } from 'vitest';

describe('floating numbers math', () => {
  test('floatAdditions', () => {
    expect(MathUtil.floatAdditions(0.1, 0.2)).toBe(0.3);
    expect(MathUtil.floatAdditions(0.1, 0.7)).toBe(0.8);
    expect(MathUtil.floatAdditions(0.7, 0.8, 0.1)).toBe(1.6);
    expect(MathUtil.floatAdditions(0.01, 0.02, 0.03)).toBe(0.06);
    expect(MathUtil.floatAdditions(0.8 + 0.2)).toBe(1);
    expect(MathUtil.floatAdditions(0.08 + 0.02)).toBe(0.1);
    expect(MathUtil.floatAdditions(0.008 + 0.002)).toBe(0.01);
    expect(MathUtil.floatAdditions(0.0008 + 0.0002)).toBe(0.001);
    expect(MathUtil.floatAdditions(0.1, 0.002)).toBe(0.102);
    expect(MathUtil.floatAdditions(53, 1000)).toBe(1053);
    expect(MathUtil.floatAdditions(-0.1, 0.1)).toBe(0);
    expect(MathUtil.floatAdditions(0.1, -0.1)).toBe(0);
    expect(MathUtil.floatAdditions(0.1, 0.2, -0.3)).toBe(0);
    expect(MathUtil.floatAdditions(0.1, 0.2, 0.3)).toBe(0.6);
    expect(MathUtil.floatAdditions(0.1, 0.2, 0.3, 0.4)).toBe(1);
    expect(MathUtil.floatAdditions(1e-20, 1e-21)).toBe(1.1e-20);
    expect(MathUtil.floatAdditions(1e-50, 1e-51)).toBe(1.1e-50);
    expect(MathUtil.floatAdditions(1.23, -0.45)).toBe(0.78);
    expect(MathUtil.floatAdditions(-2.34, -0.56)).toBe(-2.9);
    // expect(MathUtil.floatAdditions(1.23e20, 4.56e19)).toBe(1.683e20)
    // expect(MathUtil.floatAdditions(-1.23e20, -4.56e19)).toBe(-1.683e20)
    // expect(MathUtil.floatAdditions(-1e50, 1e-50)).toBe(-9.999999999999e49)
    // expect(MathUtil.floatAdditions(1e50, -1e-50)).toBe(1.0000000000001e50)

    // expect(MathUtil.floatAdditions()).toBe()
  });

  test('floatSubtraction', () => {
    expect(MathUtil.floatSubtraction(0.3, 0.2)).toBe(0.1);
    expect(MathUtil.floatSubtraction(0.8, 0.5, 0.1)).toBe(0.2);
    expect(MathUtil.floatSubtraction(0.05, 0.02, 0.01)).toBe(0.02);
    expect(MathUtil.floatSubtraction(0.1, 0.3)).toBe(-0.2);
    expect(MathUtil.floatSubtraction(53, -1000)).toBe(1053);
    expect(MathUtil.floatSubtraction(100, 99.9)).toBe(0.1);
    expect(MathUtil.floatSubtraction(-0.1, 0.1)).toBe(-0.2);
    expect(MathUtil.floatSubtraction(0.1, -0.1)).toBe(0.2);
    expect(MathUtil.floatSubtraction(0.3, 0.2, 0.1)).toBe(0);
    expect(MathUtil.floatSubtraction(0.1, 0.2, 0.3)).toBe(-0.4);
    expect(MathUtil.floatSubtraction(1, 0.5, 0.3, 0.1)).toBe(0.1);

    // expect(MathUtil.floatSubtraction()).toBe()
  });

  test('floatMultiplications', () => {
    expect(MathUtil.floatMultiplications(0.1, 0.2)).toBe(0.02);
    expect(MathUtil.floatMultiplications(0.5, 0.3, 0.1)).toBe(0.015);
    expect(MathUtil.floatMultiplications(0.01, 0.02, 0.03)).toBe(0.000006);
    expect(MathUtil.floatMultiplications(0.3, 0.0003)).toBe(0.00009);
    expect(MathUtil.floatMultiplications(100, 0.25)).toBe(25);
    expect(MathUtil.floatMultiplications(100, 4.015)).toBe(401.5);
    expect(MathUtil.floatMultiplications(0.55, 0.55)).toBe(0.3025);
    expect(MathUtil.floatMultiplications(-0.1, 0.1)).toBe(-0.01);
    expect(MathUtil.floatMultiplications(0.1, -0.1)).toBe(-0.01);
    expect(MathUtil.floatMultiplications(0.1, 0.2, -0.3)).toBe(-0.006);
    expect(MathUtil.floatMultiplications(0.1, 0.2, 0.3)).toBe(0.006);
    expect(MathUtil.floatMultiplications(1, 0.5, 0.3, 0.1)).toBe(0.015);

    // expect(MathUtil.floatMultiplications()).toBe()
  });

  test('floatDivisions', () => {
    expect(MathUtil.floatDivisions(0.2, 0.1)).toBe(2);
    expect(MathUtil.floatDivisions(0.5, 0.2, 0.1)).toBe(25);
    expect(MathUtil.floatDivisions(0.03, 0.02, 0.01)).toBe(150);
    expect(MathUtil.floatDivisions(0.9, 0.03)).toBe(30);
    expect(MathUtil.floatDivisions(100, 50)).toBe(2);
    expect(MathUtil.floatDivisions(-0.1, 0.1)).toBe(-1);
    expect(MathUtil.floatDivisions(0.1, -0.1)).toBe(-1);
    expect(MathUtil.floatDivisions(0.1, 0.2, -0.3)).toBe(-1.6666666666666667);
    expect(MathUtil.floatDivisions(0.1, 0.2, 0.3)).toBe(1.6666666666666667);
    // eslint-disable-next-line @typescript-eslint/no-loss-of-precision
    expect(MathUtil.floatDivisions(1, 0.5, 0.3, 0.1)).toBe(66.6666666666666667);

    // expect(MathUtil.floatDivisions()).toBe()
  });
});

describe('fractions()', () => {
  it('should get the fractional part of a number as a string or a number', () => {
    expect(MathUtil.fractions('-100.02')).toBe('02');
    expect(MathUtil.fractions('100.02007')).toBe('02007');
    expect(MathUtil.fractions(-100.02)).toBe(-0.02);
    expect(MathUtil.fractions(100.02007)).toBe(0.02007);
    expect(MathUtil.fractions('-0.12345')).toBe('12345');
    expect(MathUtil.fractions(1.2345)).toBe(0.2345);
    expect(MathUtil.fractions(-1.2345)).toBe(-0.2345);
  });
});
