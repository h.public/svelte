import ArrayUtil from '@common/utils/ArrayUtil.js';
import { describe, expect, it } from 'vitest';

describe('ArrayUtil', () => {
  describe('getArray', () => {
    it('should return an array with a single element if input is not an array', () => {
      const input = 123;
      const result = ArrayUtil.getArray(input);
      expect(result).toEqual([input]);
    });

    it('should not split string and return an array with a single element if input is not an array', () => {
      const input = 'string';
      const result = ArrayUtil.getArray(input);
      expect(result).toEqual([input]);
    });

    it('should return the input array if it is already an array', () => {
      const input = [1, 2, 3];
      const result = ArrayUtil.getArray(input);
      expect(result).toEqual(input);
    });

    it('should return a new deep copy of the input array if newCopy is true', () => {
      const input = [1, 2, 3];
      const result = ArrayUtil.getArray(input, true);
      expect(result).toEqual(input);
      expect(result).not.toBe(input);
    });

    it('should convert an iterable object to an array', () => {
      const iterableObject = {
        [Symbol.iterator]: function* () {
          yield 'item1';
          yield 'item2';
          yield 'item3';
        },
      };

      const result: string[] = ArrayUtil.getArray(iterableObject as string);

      expect(Array.isArray(result)).toBe(true);
      expect(result).toEqual(['item1', 'item2', 'item3']);
    });
  });

  describe('deepCopy', () => {
    it('should return a deep copy of the provided array', () => {
      const input = [1, 2, [3, 4]];
      const result = ArrayUtil.deepCopy(input);
      expect(result).toEqual(input);
      expect(result).not.toBe(input);
      expect(result[2]).not.toBe(input[2]);
    });
  });
});
