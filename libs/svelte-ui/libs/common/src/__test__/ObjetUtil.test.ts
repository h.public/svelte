import ObjectUtil from '@common/utils/ObjectUtil.js';
import { describe, expect, it } from 'vitest';

describe('ObjectUtil', () => {
  describe('deepCopy', () => {
    it('should create a deep copy of a simple object', () => {
      const inputObject = {
        a: 1,
        b: {
          c: 2,
        },
      };

      const deepCopied = ObjectUtil.deepCopy(inputObject);

      // Check if the original object is unchanged
      expect(deepCopied).toEqual(inputObject);
      expect(deepCopied).not.toBe(inputObject); // Should be a new object

      // Modify the deepCopied object and check if the original remains unchanged
      deepCopied.b.c = 3;
      expect(deepCopied.b.c).toEqual(3);
      expect(inputObject.b.c).toEqual(2);
    });

    it('should handle circular references by skipping them', () => {
      type CircularObject = {
        a: number;
        b?: CircularObject;
      };

      const circularObject: CircularObject = {
        a: 1,
      };
      circularObject.b = circularObject; // Creating a circular reference

      const deepCopied = ObjectUtil.deepCopy(circularObject);

      // Check if the circular reference is skipped and not causing an error
      expect(deepCopied.a).toEqual(1);
      expect(deepCopied.b).toEqual(deepCopied);
    });
  });

  describe('flatten', () => {
    it('should flatten a nested object with parent keys included', () => {
      const inputObject = {
        a: {
          b: {
            c: 1,
            d: 2,
          },
          e: 3,
        },
        f: 4,
      };

      const expectedResult = {
        'a.b.c': 1,
        'a.b.d': 2,
        'a.e': 3,
        f: 4,
      };

      const flattened = ObjectUtil.flatten(inputObject);

      expect(flattened).toEqual(expectedResult);
    });

    it('should flatten a nested object with parent keys excluded', () => {
      const inputObject = {
        a: {
          b: {
            c: 1,
            d: 2,
          },
          e: 3,
        },
        f: 4,
      };

      const expectedResult = {
        c: 1,
        d: 2,
        e: 3,
        f: 4,
      };

      const flattened = ObjectUtil.flatten(inputObject, false);

      expect(flattened).toEqual(expectedResult);
    });
  });
});
