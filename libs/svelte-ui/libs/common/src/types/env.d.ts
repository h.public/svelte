/**
 * Declare constants for environment variables
 *
 * In your package.json scripts, you can define an environment variable when running the build
 * command using a build tool like Vite or Webpack.
 *
 * For example, in Vite, you can define the DEV environment variable using 'mode':
 *
 * "scripts": { "dev": "vite --mode=DEV" }
 *
 * Then, in your Vite configuration file (vite.config.ts), you can use the replace plugin to replace
 * values of DEV, SIT, and UAT, like this:
 *
 * Import replace from '@rollup/plugin-replace';
 *
 * Export default defineConfig({ plugins: [ replace({ preventAssignment: true, values: { 'env.DEV':
 * JSON.stringify(mode === 'DEV'), 'env.SIT': JSON.stringify(mode === 'SIT'), 'env.UAT':
 * JSON.stringify(mode === 'UAT'), }, });
 *
 * This will replace occurrences of DEV, SIT, and UAT in your code with the specified boolean values
 * (in this case, false). You can customize the replacement values based on your specific
 * requirements.
 *
 * Note: Make sure to update the values of these constants accordingly based on your actual build
 * process and environment configurations.
 *
 * @example
 *   if (env.DEV) {
 *     console.log('In development environment');
 *   }
 */
declare const env: {
  DEV: boolean; // Represents the development environment
  SIT: boolean; // Represents the staging environment
  UAT: boolean; // Represents the user acceptance testing environment
};
