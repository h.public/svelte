/**
 * A callback function that takes optional parameter and returns no value.
 *
 * @template T - Optional type parameter.
 * @callback Callback
 * @param {T} [value] - Optional value parameter.
 * @returns {void}
 */
type Callback<T = void> = (value?: T) => void;

/**
 * A callback function that takes a value of type T as a parameter and returns no value.
 *
 * @template T - The type of the value parameter.
 * @callback CallbackWith
 * @param {T} value - The value parameter.
 * @returns {void}
 */
type CallbackWith<T> = (value: T) => void;

/**
 * Represents a type that can be null or undefined in addition to the original type.
 *
 * @example
 *   type MaybeString = Nullable<string>; // MaybeString can be a string, null or undefined
 *
 * @template T - The original type to be nullable.
 */
type Nullable<T> = T | null | undefined;

/**
 * Represents a type that has certain properties required.
 *
 * @example
 *   interface Person {
 *     name?: string;
 *     age?: number;
 *     email?: string;
 *   }
 *
 *   type RequiredPerson = WithRequired<Person, 'name' | 'age'>;
 *   // ? RequiredPerson is an interface with the same properties as Person,
 *   // ? but with the 'name' and 'age' properties required.
 *
 * @template T - The original type to be augmented with required properties.
 * @template K - The keys of the properties that should be made required.
 */
type WithRequired<T, K extends keyof T> = T & { [P in K]-?: T[P] };

/**
 * Represents a type that has readonly properties that can be made mutable.
 *
 * @example
 *   interface Person {
 *     readonly name: string;
 *     readonly age: number;
 *     readonly email: string;
 *   }
 *
 *   type MutablePerson = Mutable<Person>;
 *   // ? MutablePerson is an interface with the same properties as Person,
 *   // ? but with all properties made mutable.
 *
 * @template T - The original type to be made mutable.
 */
type Mutable<T> = {
  -readonly [K in keyof T]: T[K];
};

/**
 * Combines two types, `T` and `TOverridden`, by overriding properties from `T` with properties from
 * `TOverridden`. The resulting type contains all properties from `TOverridden`, and any remaining
 * properties from `T` that are not present in `TOverridden`.
 *
 * @example
 *   // Define a base type
 *   interface Person {
 *     name: string;
 *     age: number;
 *   }
 *
 *   // Define an overridden type
 *   interface PersonOverride {
 *     age: string; // Override the 'age' property to be a string instead of a number
 *     gender: string; // Add a new 'gender' property
 *   }
 *
 *   // Use OverrideProps to combine the types
 *   type OverriddenPerson = OverrideProps<Person, PersonOverride>;
 *
 *   // The resulting type will have 'name' as string, 'age' as string, and 'gender' as string
 *   const person: OverriddenPerson = {
 *     name: 'John Doe',
 *     age: '30',
 *     gender: 'male',
 *   };
 *
 * @template T - The base type.
 * @template TOverridden - The type containing overridden properties.
 */
type OverrideProps<T, TOverridden> = Omit<T, keyof TOverridden> & TOverridden;

/**
 * Represents an object type with unknown properties.
 *
 * @example
 *   const myObj: UnknownRecord = { foo: 'bar', baz: 123 };
 *   // ? myObj can have any number of properties of any type, as long as they are strings.
 */
type UnknownRecord = Record<string, unknown>;
