/**
 * Determines whether the given value is a boolean or not.
 *
 * @memberof DataTypeHelper
 * @param {unknown} value - The value to check.
 * @returns {boolean} - `true` if the value is a boolean, `false` otherwise.
 */
export const isBoolean = (value: unknown) => {
  return typeof value === 'boolean' || value === 'true' || value === 'false';
};

/**
 * Determines whether the given value is a valid number or not.
 *
 * @memberof DataTypeHelper
 * @param {string | number} value - The value to check.
 * @returns {boolean} - `true` if the value is a valid number, `false` otherwise.
 */
export const isNumber = (value: string | number) => {
  if (typeof value === 'boolean' || value === null || Array.isArray(value)) return false;
  return typeof +value === 'number' && !isNaN(+value);
};

/**
 * Determines whether the given value is a string or not.
 *
 * @memberof DataTypeHelper
 * @param {unknown} value - The value to check.
 * @returns {boolean} - `true` if the value is a string, `false` otherwise.
 */
export const isString = (value: unknown) => {
  return typeof value === 'string' || '' + value === value;
};

/**
 * Determines whether the given value is a valid JSON string or not.
 *
 * @memberof DataTypeHelper
 * @param {string | JSON} value - The value to check.
 * @returns {boolean} - `true` if the value is a valid JSON string, `false` otherwise.
 */
export const isJson = (value: string | JSON) => {
  if (typeof value !== 'string' && !(value instanceof String)) {
    return false;
  }
  try {
    JSON.parse(value as string);
    return true;
  } catch (e) {
    return false;
  }
};

/**
 * Determines whether the given value is an array or not.
 *
 * @memberof DataTypeHelper
 * @param {unknown} value - The value to check.
 * @returns {boolean} - `true` if the value is an array, `false` otherwise.
 */
export const isArray = (value: unknown) => {
  return Array.isArray(value);
};

/**
 * Determines whether the given value is an object or not.
 *
 * @memberof DataTypeHelper
 * @param {unknown} value - The value to check.
 * @returns {boolean} - `true` if the value is an object, `false` otherwise.
 */
export const isObject = (value: unknown) => {
  return (
    typeof value === 'object' &&
    value !== null &&
    !Array.isArray(value) &&
    !(value instanceof Date) &&
    !(value instanceof RegExp)
  );
};

/**
 * Determines whether the given value is a function or not.
 *
 * @memberof DataTypeHelper
 * @param {unknown} value - The value to check.
 * @returns {boolean} - `true` if the value is a function, `false` otherwise.
 */
export const isFunction = (value: unknown) => {
  return typeof value === 'function';
};

/**
 * A helper class for common data type checks and conversions.
 *
 * @namespace DataTypeHelper
 */
const DataTypeHelper = {
  /** @see {@link isBoolean} */
  isBoolean,
  /** @see {@link isNumber} */
  isNumber,
  /** @see {@link isString} */
  isString,
  /** @see {@link isJson} */
  isJson,
  /** @see {@link isArray} */
  isArray,
  /** @see {@link isObject} */
  isObject,
  /** @see {@link isFunction} */
  isFunction,
};

export default DataTypeHelper;
