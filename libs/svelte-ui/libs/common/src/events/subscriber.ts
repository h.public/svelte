/**
 * Creates a generic subscriber utility.
 *
 * @example
 *   // Create a subscriber for number values
 *   const numberSubscriber = createSubscriber<number>();
 *
 *   // Subscribe to value changes
 *   numberSubscriber.subscribe((value) => {
 *     console.log('Number:', value);
 *   });
 *
 *   // Notify subscribers of a new value
 *   numberSubscriber.notifySubscribers(42); // Logs: Number: 42
 *
 *   // Unsubscribe from value changes
 *   numberSubscriber.unsubscribe(callback);
 *
 * @template T - The type of the value to be subscribed to.
 * @returns {Object} The subscriber object.
 */
export const createSubscriber = <T>() => {
  /** @type {Set<CallbackWith<T>>} */
  const subscribers = new Set<CallbackWith<T>>();

  /**
   * Subscribes to value changes.
   *
   * @param {CallbackWith<T>} callback - The callback function to be invoked on value changes.
   */
  const subscribe = (callback: CallbackWith<T>) => {
    subscribers.add(callback);
  };

  /**
   * Unsubscribes from value changes.
   *
   * @param {CallbackWith<T>} callback - The callback function to be removed from subscriptions.
   */
  const unsubscribe = (callback: CallbackWith<T>) => {
    subscribers.delete(callback);
  };

  /**
   * Notifies all subscribers of value changes.
   *
   * @param {T} value - The new value to be passed to subscribers.
   */
  const notifySubscribers = (value: T) => {
    for (const subscriber of subscribers) {
      subscriber(value);
    }
  };

  /** Clears all subscribers from the subscriber set. */
  const clear = () => {
    subscribers.clear();
  };

  return {
    subscribe,
    unsubscribe,
    notifySubscribers,
    clear,
  };
};
