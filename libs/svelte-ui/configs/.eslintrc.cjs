/**
 * Resolves the ESLint configuration file path based on the current directory location.
 *
 * @module ESLintConfigResolver
 */

const path = require('path');
const currentDirectory = path.dirname(__filename);
const isUnderLibs = currentDirectory.includes(path.sep + 'libs' + path.sep);
const baseDir = isUnderLibs ? '../../../' : '..';

module.exports = {
  extends: [path.resolve(__dirname, baseDir + '/libs/common/configs/.eslintrc')],
  env: {
    browser: true,
  },
};
