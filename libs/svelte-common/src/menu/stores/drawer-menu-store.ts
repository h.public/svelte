import { writable } from 'svelte/store';

/**
 * A Svelte writable store representing the state of the checkbox.
 *
 * @type {import('svelte/store').Writable<boolean>}
 */
export const isOpened = writable(false);

/**
 * Toggle the state of the checkbox.
 *
 * @example
 *   // Initial state: isOpened = false
 *   toggle(); // isOpened is set to true
 *
 *   // Explicitly set to false
 *   toggle(false); // isOpened is set to false
 *
 *   // Explicitly set to true
 *   toggle(true); // isOpened is set to true
 *
 * @param {boolean} [active] - Optional parameter to explicitly set the checkbox state.
 */
const toggle = (active?: boolean) => {
  isOpened.set(active ?? !isOpened);
};

/**
 * An object representing the drawer menu store with its state and toggle function.
 *
 * @namespace
 * @property {import('svelte/store').Writable<boolean>} isOpened - The checkbox state.
 * @property {Function} toggle - The function to toggle the checkbox state.
 */
export const drawerMenuStore = {
  ...isOpened,
  /** @see {@link toggle} */
  toggle,
};
