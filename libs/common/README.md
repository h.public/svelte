# Common TypeScript Library

This is a collection of TypeScript code for common usage. The library can be added to your project as a Git submodule, allowing you to easily reuse the code across multiple projects.

## Using the Library as a Git Submodule

To add this library as a Git submodule to your project, run the following command:

```
git submodule add https://gitlab.com/h.library/common.git libs/common
```

To update the Git submodule to the latest version, you can use the following command:

```
git submodule update --remote
```

## Configuring the Library in tsconfig

To configure path aliases in your TypeScript project, you can modify your tsconfig.json file with the following rules:

```
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@common/*": [
        "libs/common/src/*"
      ],
      "#framework/*": [
        "frameworks/framework/src/*"
      ],
      "$service/*": [
        "services/service/src/*"
      ],
      "~/*": [
        "src/*"
      ]
    }
  }
}
```

Note that the baseUrl property should be set to the root directory of your project.
