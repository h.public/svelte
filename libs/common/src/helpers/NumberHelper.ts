/**
 * Restricts the given number to a specified range.
 *
 * @param {number} num - The number to clamp.
 * @param {number} a - One end of the range.
 * @param {number} b - The other end of the range.
 * @returns {number} The clamped number.
 */
export const clamp = (num: number, a: number, b: number) => {
  return Math.max(Math.min(num, Math.max(a, b)), Math.min(a, b));
};

/**
 * Switches a positive number to a negative number, or a negative number to a positive number.
 *
 * @param {number} num - The number to switch.
 * @returns {number} The switched number.
 */
export const switchPositiveNegative = (num: number) => {
  return num > 0 ? num * -1 : Math.abs(num);
};

/**
 * Switches a positive number to a negative number.
 *
 * @param {number} num - The positive number to switch.
 * @returns {number} The negative number.
 */
export const positiveToNegative = (num: number) => {
  return num * -1;
};

/**
 * Switches a negative number to a positive number.
 *
 * @param {number} num - The negative number to switch.
 * @returns {number} The positive number.
 */
export const negativeToPositive = (num: number) => {
  return Math.abs(num);
};

/**
 * Formats a number with a specified precision and thousands separator.
 *
 * @param {string | number} num - The number to format.
 * @param {number} [precision=2] - The number of decimal places to round the number to. Default is
 *   `2`. Default is `2`
 * @param {string} [thousandsSeparator=','] - The character used as the thousands separator. Default
 *   is `','`. Default is `','`
 * @returns {string} The formatted number as a string.
 */
export const formatNumber = (num: string | number, precision = 2, thousandsSeparator = ',') => {
  const formattedNumber = trunc(+num, precision)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparator);
  return formattedNumber;
};

/**
 * Truncates a number to a certain precision.
 *
 * @param {number} num - The number to truncate.
 * @param {number} [precision=0] - The number of decimal places to truncate to. Default is `0`
 * @param {Trunc} [trunc=Trunc.CLEAN] - The truncation mode. Can be "Trunc.CLEAN", "Trunc.ROUND", or
 *   "Trunc.FLOOR". Default is `Trunc.CLEAN`
 * @returns {number} The truncated number.
 */
export const trunc = (num: number, precision = 0, trunc: Trunc = Trunc.CLEAN) => {
  // no need to handle a number without decimal
  // as the number will be trunc in CLEAN mode
  if (num % 1 === 0) return num;

  if (trunc === Trunc.CLEAN) {
    if (precision === 0) {
      return Math.trunc(num);
    }

    const parts: string[] = num.toString().split('.');
    if (parts.length === 2) {
      return +[parts[0], parts[1].slice(0, precision)].join('.');
    } else {
      return +parts[0];
    }
  }

  const m: number = 10 ** precision; // Math.pow(10, precision);
  switch (trunc) {
    case Trunc.ROUND:
      return Math.round(num * m) / m;
    case Trunc.FLOOR:
      return Math.floor(num * m) / m;
  }
};

/**
 * Checks if the provided number has at most one leading zero before the decimal point.
 *
 * @param {string | number} num - The number to be validated.
 * @returns {boolean} Returns true if the number has at most one leading zero before the decimal
 *   point, false otherwise.
 */
export const noLeadingZeroes = (num: string | number) => {
  // Special handling
  if (num.toString() === '0') return true;

  /*
    ^ asserts the start of the string.
    (?!0\d) is a negative lookahead that ensures there is no zero followed by a digit at the beginning.
    \d* matches zero or more digits.
    \.? matches an optional decimal point.
    \d+ matches one or more digits after the decimal point (if present).
    $ asserts the end of the string.
  */
  return /^(?!0\d)\d*\.?\d+$/.test(num.toString());
};

/**
 * A helper class for number validation.
 *
 * @namespace NumberHelper
 */
const NumberHelper = {
  /** @see {@link clamp} */
  clamp,
  /** @see {@link switchPositiveNegative} */
  switchPositiveNegative,
  /** @see {@link positiveToNegative} */
  positiveToNegative,
  /** @see {@link negativeToPositive} */
  negativeToPositive,
  /** @see {@link formatNumber} */
  formatNumber,
  /** @see {@link trunc} */
  trunc,
  /** @see {@link noLeadingZeroes} */
  noLeadingZeroes,
};

export default NumberHelper;

export const enum Trunc {
  CLEAN,
  ROUND,
  FLOOR,
}
