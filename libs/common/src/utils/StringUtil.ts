/**
 * A utility class for working with strings.
 *
 * @namespace StringUtil
 */
const StringUtil = {
  /**
   * Returns everything after the first occurrence of the provided character in the string.
   *
   * @param {string} string - The string.
   * @param {string} char - The character or sub-string.
   * @returns {string} - The substring after the first occurrence of `char`.
   */
  afterFirst(string: string, char: string) {
    if (!string || !string.includes(char)) {
      return '';
    }
    let idx: number = string.indexOf(char);
    idx += char.length;
    return string.substring(idx);
  },

  /**
   * Returns everything after the last occurrence of the provided character in string.
   *
   * @param {string} string - The string.
   * @param {string} char - The character or sub-string.
   * @returns {string} - The substring after the last occurrence of `char`.
   */
  afterLast(string: string, char: string) {
    if (!string || !string.includes(char)) {
      return '';
    }
    let idx: number = string.lastIndexOf(char);
    idx += char.length;
    return string.substring(idx);
  },

  /**
   * Returns everything before the first occurrence of the provided character in the string.
   *
   * @param {string} string - The string.
   * @param {string} char - The character or sub-string.
   * @returns {string} - The substring before the first occurrence of `char`.
   */
  beforeFirst(string: string, char: string) {
    if (!string || !string.includes(char)) {
      return '';
    }
    const idx: number = string.indexOf(char);
    return string.substring(0, idx);
  },

  /**
   * Returns everything before the last occurrence of the provided character in the string.
   *
   * @param {string} string - The string.
   * @param {string} char - The character or sub-string.
   * @returns {string} - The substring before the last occurrence of `char`.
   */
  beforeLast(string: string, char: string) {
    if (!string || !string.includes(char)) {
      return '';
    }
    const idx: number = string.lastIndexOf(char);
    return string.substring(0, idx);
  },

  /**
   * Returns everything after the first occurrence of `start` and before the first occurrence of
   * `end` in `string`.
   *
   * @param {string} string - The string.
   * @param {string} start - The character or sub-string to use as the start index.
   * @param {string} end - The character or sub-string to use as the end index.
   * @returns {string} - The substring between `start` and `end`.
   */
  between(string: string, start: string, end: string) {
    const str = '';
    if (!string) {
      return str;
    }
    const startIdx = string.indexOf(start) + start.length;
    return string.substring(startIdx, string.indexOf(end, startIdx));
  },

  /**
   * Capitalizes the first letter in a string or all words.
   *
   * @param {string} string - The string.
   * @param {boolean} [allWords=false] - Indicating if we should capitalize all words or only the
   *   first. Default is `false`
   * @returns {string}
   */
  capitalize(string: string, allWords = false) {
    const str: string = string.trimStart();
    if (allWords) {
      return str.toUpperCase();
    } else {
      return str.charAt(0).toUpperCase() + str.slice(1);
    }
  },

  /**
   * Determines the number of times a character or sub-string appears within the string.
   *
   * @param {string} string - The string.
   * @param {string} char - The character or sub-string to count.
   * @param {boolean} [caseSensitive=true] - Indicate if the search is case sensitive. Default is
   *   `true`. Default is `true`
   * @returns {number}
   */
  countOf(string: string, char: string, caseSensitive = true) {
    if (!string) {
      return 0;
    }
    const newChar: string = escapePattern(char);
    const flags: string = !caseSensitive ? 'ig' : 'g';
    return string.match(new RegExp(newChar, flags))?.length || 0;
  },

  /**
   * Levenshtein distance (editDistance) is a measure of the similarity between two strings. The
   * distance is the number of deletions, insertions, or substitutions required to transform string
   * into target.
   *
   * @param {string} string - The source string.
   * @param {string} target - The target string.
   * @returns {number}
   */
  editDistance(string: string, target: string) {
    let i: number;
    let j: number;

    if (!string) {
      string = '';
    }
    if (target === null) {
      target = '';
    }

    if (string === target) {
      return 0;
    }

    const d: number[][] = [];
    let cost: number;
    const n: number = string.length;
    const m: number = target.length;

    if (n === 0) {
      return m;
    }
    if (m === 0) {
      return n;
    }

    for (i = 0; i <= n; i++) {
      d[i] = [];
    }
    for (i = 0; i <= n; i++) {
      d[i][0] = i;
    }
    for (j = 0; j <= m; j++) {
      d[0][j] = j;
    }

    for (i = 1; i <= n; i++) {
      const si: string = string.charAt(i - 1);
      for (j = 1; j <= m; j++) {
        const tj: string = target.charAt(j - 1);

        if (si === tj) {
          cost = 0;
        } else {
          cost = 1;
        }

        d[i][j] = minimum(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
      }
    }
    return d[n][m];
  },

  /**
   * Determines whether the specified string contains text.
   *
   * @param {string} string - The string to check.
   * @returns {boolean}
   */
  hasText(string: string) {
    const str: string = this.removeExtraWhitespace(string);
    return !!str.length;
  },

  /**
   * Determines whether the specified string contains any characters.
   *
   * @param {string} string - The string to check
   * @returns {boolean}
   */
  isEmpty(string: string) {
    if (!string) {
      return true;
    }
    return !string.trim().length;
  },

  /**
   * Determines whether the specified string is numeric.
   *
   * @param {string} string - The string.
   * @returns {boolean}
   */
  isNumeric(string: string) {
    if (!string) {
      return false;
    }
    const regx = /^[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?$/;
    return regx.test(string);
  },

  /**
   * Properly cases' the string in "sentence format".
   *
   * @param {string} string - The string to check.
   * @returns {string}
   */
  properCase(string: string) {
    if (!string) {
      return '';
    }
    const str: string = string.toLowerCase().replace(/\b([^.?;!]+)/, this.capitalize);
    return str.replace(/\b[i]\b/, 'I');
  },

  /**
   * Escapes all of the characters in a string to create a friendly "quotable" string.
   *
   * @param {string} string - The string that will be checked for instances of remove string.
   * @returns {string}
   */
  quote(string: string) {
    const regx = /[\\"\r\n]/g;
    return '"' + string.replace(regx, _quote) + '"'; // "
  },

  /**
   * Removes all instances of the remove string in the input string.
   *
   * @param {string} string - The string that will be checked for instances of remove string.
   * @param {string} remove - The string that will be removed from the input string.
   * @param {boolean} [caseSensitive=true] - Indicating if the replace is case sensitive. Default is
   *   `true`. Default is `true`
   * @returns {string} - The modified string.
   */
  remove(string: string, remove: string, caseSensitive = true) {
    if (!string) {
      return '';
    }
    const rem: string = escapePattern(remove);
    const flags: string = !caseSensitive ? 'ig' : 'g';
    return string.replace(new RegExp(rem, flags), '');
  },

  /**
   * Removes extraneous whitespace (extra spaces, tabs, line breaks, etc) from the specified string.
   *
   * @param {string} string - The string whose extraneous whitespace will be removed.
   * @returns {string} - The modified string.
   */
  removeExtraWhitespace(string: string) {
    if (!string) {
      return '';
    }
    const str: string = string.trimStart();
    return str.replace(/\s+/g, ' ');
  },

  /**
   * Returns the specified string in reverse character order.
   *
   * @param {string} string - The string that will be reversed.
   * @returns {string} - The modified string.
   */
  reverse(string: string) {
    if (!string) {
      return '';
    }
    return string.split('').reverse().join('');
  },

  /**
   * Returns the specified string in reverse word order.
   *
   * @param {string} string - The string that will be reversed.
   * @returns {string} - The modified string.
   */
  reverseWords(string: string) {
    if (!string) {
      return '';
    }
    return string.split(/\s+/).reverse().join('');
  },

  /**
   * Determines the percentage of similarity, based on editDistance.
   *
   * @param {string} string - The source string.
   * @param {string} target - The target string.
   * @returns {number} - The percentage of similarity.
   */
  similarity(string: string, target: string) {
    const ed: number = this.editDistance(string, target);
    const maxLen: number = Math.max(string.length, target.length);
    if (maxLen === 0) {
      return 100;
    } else {
      return (1 - ed / maxLen) * 100;
    }
  },

  /**
   * Swaps the casing of a string.
   *
   * @param {string} string - The source string.
   * @returns {string} - The modified string.
   */
  swapCase(string: string) {
    if (!string) {
      return '';
    }
    return string.replace(/(\w)/, this.swapCase);
  },

  /**
   * Determine the number of words in a string.
   *
   * @param {string} string - The string.
   * @returns {number} - The number of words in the string.
   */
  wordCount(string: string) {
    if (!string) {
      return 0;
    }
    return string.match(/\b\w+\b/g)?.length || 0;
  },

  /**
   * Returns a string truncated to a specified length with optional suffix.
   *
   * @param {string} string - The string.
   * @param {number} len - The length the string should be shortened to.
   * @param {string} [suffix='...'] - The string to append to the end of the truncated string.
   *   Default is `'...'`
   * @param {boolean} [useSmart=false] - Whether to use smart truncation. Default is `false`
   * @returns {string} - The truncated string.
   */
  truncate(string: string, len: number, suffix = '...', useSmart = false) {
    if (!string) {
      return '';
    }

    if (isNaN(len)) {
      len = string.length;
    }

    len -= suffix.length;

    let trunc: string = string;

    if (trunc.length > len) {
      trunc = trunc.substring(0, len);

      if (useSmart) {
        if (/[^\s]/.test(string.charAt(len))) {
          trunc = trunc.replace(/\w+$|\s+$/, '').trimEnd();
        }
      }

      trunc += suffix;
    }

    return trunc;
  },

  /**
   * Returns a string or number as a formatted number with commas.
   *
   * @param {number} num - The number to be formatted.
   * @returns {string} The formatted number string.
   */
  numberWithCommas(num: string | number) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  },
};

/* **************************************************************** */
/*	These are helper methods used by some of the above methods.		  */
/* **************************************************************** */
function escapePattern(pattern: string) {
  // RM: might expose this one, I've used it a few times already.
  return pattern.replace(/(\]|\[|\{|\}|\(|\)|\*|\+|\?|\.|\\)/g, '\\$1');
}

function minimum(a: number, b: number, c: number) {
  return Math.min(a, Math.min(b, Math.min(c, a)));
}

function _quote(string: string) {
  switch (string) {
    case '\\':
      return '\\\\';
    case '\r':
      return '\\r';
    case '\n':
      return '\\n';
    case '"':
      return '\\"';
    default:
      return '';
  }
}

/* private static _swapCase(char: string): string
{
    const lowChar: string = char.toLowerCase();
    const upChar: string = char.toUpperCase();
    switch (char)
    {
        case lowChar:
            return upChar;
        case upChar:
            return lowChar;
        default:
            return char;
    }
}*/

export default StringUtil;
