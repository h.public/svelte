/**
 * A utility module for generating random values.
 *
 * @module RandomUtil
 */
const RandomUtil = {
  /**
   * Generates a random number from 0 up to a maximum value.
   *
   * @param {number} max - The maximum value for the random number.
   * @param {number} [precision=0] - The number of decimal places for the random number. Default is
   *   `0`. Default is `0`
   * @returns {number} A random number.
   */
  number(max: number, precision = 0) {
    return this.range(0, max, precision);
  },

  /**
   * Generates a random number within a given range.
   *
   * @param {number} min - The minimum value for the range.
   * @param {number} max - The maximum value for the range (exclusive).
   * @param {number} [precision=0] - The number of decimal places for the random number. Default is
   *   `0`. Default is `0`
   * @returns {number} A random number within the range.
   */
  range(min: number, max: number, precision = 0) {
    if (min === max) {
      return min;
    }

    if (precision === 0) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    const dp = 10 ** precision;
    return Math.floor((Math.random() * (max - min) + min) * dp) / dp;
  },

  /**
   * Returns a random positive or negative number.
   *
   * @param {number} num - The absolute value of the number.
   * @returns {number} A random positive or negative number.
   */
  positiveNegative(num: number) {
    return Math.random() < 0.5 ? -num : num;
  },

  /**
   * Generates a random color.
   *
   * @returns {number} A random color as a hexadecimal number.
   */
  color() {
    return Math.floor(Math.random() * 0xffffff);
  },

  /**
   * Generates a random boolean value.
   *
   * @returns {boolean} A random boolean value.
   */
  boolean() {
    return Math.random() > 0.5;
  },
};

export default RandomUtil;
