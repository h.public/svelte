import replace from '@rollup/plugin-replace';
import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
  plugins: [
    tsconfigPaths(),
    sveltekit(),
    replace({
      preventAssignment: true,
      values: {
        'env.DEV': JSON.stringify(mode === 'DEV'),
        'env.SIT': JSON.stringify(mode === 'SIT'),
        'env.UAT': JSON.stringify(mode === 'UAT'),
      },
    }),
  ],
  server: {
    fs: {
      // Allow serving files from one level up to the project root
      allow: ['.'],
    },
  },
  css: {
    postcss: './configs/postcss.config.js',
  },
}));
