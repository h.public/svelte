const topSellers = [
  {
    image: '1.fcc6789c.png',
  },
  {
    image: '2.3ce51c9b.png',
  },
  {
    image: '3.04ed945e.png',
  },
  {
    image: '4.094f7bca.png',
  },
  {
    image: '5.8cd5406d.png',
  },
  {
    image: '6.495b0a76.png',
  },
  {
    image: '7.2aa4eb57.png',
  },
  {
    image: '8.1a13dece.png',
  },
  {
    image: '9.bacac318.png',
  },
  {
    image: '10.9240c973.png',
  },
];

const topCollections = [
  {
    image: 'card-item-2.bfe0307457e2f66fb932.jpg',
  },
  {
    image: 'card-item-3.f5958cc952bce34d7810.jpg',
  },
  {
    image: 'card-item-4.8b28f05584c4388a3162.jpg',
  },
  {
    image: 'card-item-7.7f4ad6d33deea16f00f9.jpg',
  },
  {
    image: 'card-item8.7eabfc1f4f02ba299fe9.jpg',
  },
  {
    image: 'card-item-9.eee24c93e6039eff9c41.jpg',
  },
  {
    image: 'image-box-6.52d8a8ffbd8d4727e300.jpg',
  },
  {
    image: 'image-box-11.8bb5204cf47ba79610d0.jpg',
  },
];

export const nft = {
  topSellers,
  topCollections,
};
