export const verifiedCommunities = [
  {
    title: 'title1',
    image: 'src/lib/images/game-icons/GWCxvLVQw6o1Sct.webp',
    url: '#',
  },
  { title: 'title2', image: 'src/lib/images/game-icons/lost-survivors.webp', url: '#' },
  {
    title: 'title3',
    image: 'src/lib/images/game-icons/Paid_331_is.radiantgames.boxisland.paymium.jpg',
    url: '#',
  },
  { title: 'title4', image: 'src/lib/images/game-icons/run-island.jpg', url: '#' },
  {
    title: 'title1',
    image: 'src/lib/images/game-icons/GWCxvLVQw6o1Sct.webp',
    url: '#',
  },
  { title: 'title2', image: 'src/lib/images/game-icons/lost-survivors.webp', url: '#' },
  {
    title: 'title3',
    image: 'src/lib/images/game-icons/Paid_331_is.radiantgames.boxisland.paymium.jpg',
    url: '#',
  },
];

export const unverifiedCommunities = [
  {
    title: 'title1',
    image: 'src/lib/images/game-icons/GWCxvLVQw6o1Sct.webp',
    url: '#',
  },
  { title: 'title2', image: 'src/lib/images/game-icons/lost-survivors.webp', url: '#' },
];
