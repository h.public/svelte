import RandomUtil from '@common/utils/RandomUtil.js';

export const games = [
  {
    image:
      'src/lib/images/games/26BR_C4S4_EGS_Launcher_Blade_1200x1600_1200x1600-72d477839e2f1e1a9b3847d0998f50bc.webp',
    link: '#',
  },
  {
    image:
      'src/lib/images/games/EGS_DeadbyDaylight_BehaviourInteractive_S2_1200x1600-a1c30209e3b9fb63144daa9b5670f503.webp',
    link: '#',
  },
  {
    image:
      'src/lib/images/games/EGS_GodofWar_SantaMonicaStudio_S2_1200x1600-fbdf3cbc2980749091d52751ffabb7b7_1200x1600-fbdf3cbc2980749091d52751ffabb7b7.webp',
    link: '#',
  },
  {
    image:
      'src/lib/images/games/EGS_HogwartsLegacy_AvalancheSoftware_S2_1200x1600-2bb94423bf1c7e2fca10577d9f2878b9.webp',
    link: '#',
  },
  {
    image:
      'src/lib/images/games/EGS_MarvelsSpiderManMilesMorales_InsomniacGamesNixxesSoftware_S2_1200x1600-58989e7116de3f70a2ae6ea56ee202c6.webp',
    link: '#',
  },
  {
    image:
      'src/lib/images/games/EGS_Returnal_HousemarqueClimaxStudios_S2_1200x1600-617cb4daee4cf45fa937304bceb60a81.webp',
    link: '#',
  },
  {
    image:
      'src/lib/images/games/EGS_RocketLeague_PsyonixLLC_S2_1200x1600-edcc4af1ea991205eb2346c6691cc99c.webp',
    link: '#',
  },
  {
    image:
      'src/lib/images/games/EGS_TheLastofUsPartI_NaughtyDogLLC_S2_1200x1600-41d1b88814bea2ee8cb7986ec24713e0.webp',
    link: '#',
  },
  {
    image: 'src/lib/images/games/the-lord-of-the-rings-return-to-moria-tr0c9.png',
    link: '#',
  },
  {
    image: 'src/lib/images/games/witchfire-obly4.jpg',
    link: '#',
  },
];

export const trending = games.filter((game) => RandomUtil.boolean());
export const popular = games.filter((game) => RandomUtil.boolean());
export const upcoming = games.filter((game) => RandomUtil.boolean());

export const categories = [
  { category: 'Trending', link: '#' },
  { category: 'Popular', link: '#' },
  { category: 'Upcoming', link: '#' },
];
