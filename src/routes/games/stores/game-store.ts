import { writable } from 'svelte/store';

export const currentCategory = writable<Nullable<string>>();
