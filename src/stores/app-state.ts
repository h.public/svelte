import { writable } from 'svelte/store';

export let currentPath = writable<Nullable<boolean>>(false);
