import { writable } from 'svelte/store';

export const triggerApp = writable<Nullable<boolean>>(false);
export const enterWorld = writable<Nullable<boolean>>(false);
