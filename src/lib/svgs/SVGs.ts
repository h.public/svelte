const socials = {
  facebook: 'facebook.svg',
  telegram: 'telegram.svg',
  tiktok: 'tiktok.svg',
  whatsapp: 'whatsapp.svg',
  youtube: 'youtube.svg',
} as const;

const common = {
  language: 'language.svg',
  other: 'other.svg',
  backToTop: 'up-arrow.svg',
  search: 'search.svg',
} as const;

const theme = {
  dark: 'moon.svg',
  light: 'sun.svg',
} as const;

export const SVGs = {
  account: 'account.svg',
  common,
  socials,
  theme,
} as const;

const parse = (key: string, path: string) => `./src/lib/svgs/${key.length ? key + '/' : ''}` + path;

Object.entries(SVGs).forEach(([key, value]) => {
  if (typeof value === 'string') {
    SVGs[key] = parse('', value) as any;
    return;
  }
  Object.entries(value).forEach(([key2, value2]) => {
    value[key2] = parse(key, value2) as any;
  });
});
