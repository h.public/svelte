const islands = [
  {
    name: 'Candy Island',
    url: '/world#candy',
  },
  {
    name: 'Choco Island',
    url: '/world#choco',
  },
  {
    name: 'Snow Island',
    url: '/world#snow',
  },
  {
    name: 'Donut Island',
    url: '/world#donut',
  },
  {
    name: 'Cake Island',
    url: '/world#cake',
  },
];

const games = [
  {
    name: 'Candy Island',
    url: '#',
  },
  {
    name: 'Choco Island',
    url: '#',
  },
  {
    name: 'Snow Island',
    url: '#',
  },
  {
    name: 'Donut Island',
    url: '#',
  },
  {
    name: 'Cake Island',
    url: '#',
  },
];

const nfts = [
  {
    name: 'NFT main tab',
    url: '#',
  },
  {
    name: 'Top Creators',
    url: '#',
  },
  {
    name: 'What is NFT?',
    url: '#',
  },
  {
    name: 'Help Centre',
    url: '#',
  },
];

const tokens = [
  {
    name: 'Tokens main tab',
    url: '#',
  },
  {
    name: 'Token sub tab',
    url: '#',
  },
  {
    name: 'Token market',
    url: '#',
  },
  {
    name: 'Token Resource',
    url: '#',
  },
];

const socials = [
  {
    name: 'Community',
    url: '/community',
  },
  {
    name: 'Facebook',
    url: '#',
  },
  {
    name: 'Twitter',
    url: '#',
  },
  {
    name: 'Discord',
    url: '#',
  },
  {
    name: 'Telegram',
    url: '#',
  },
];

const resources = {
  label: 'Resources',
  links: [
    {
      name: 'Distribute on Islands',
      url: '#',
    },
    {
      name: 'Community Rules',
      url: '#',
    },
    {
      name: 'Newsroom',
      url: '/newsroom',
    },
    {
      name: 'Developer Center',
      url: '#',
    },
  ],
};

const supports = {
  label: 'Support',
  links: [
    {
      name: 'Contact Support',
      url: '#',
    },
    {
      name: 'FAQ',
      url: '#',
    },
    {
      name: 'Tutorial',
      url: '#',
    },
  ],
};

const terms = {
  label: 'Terms & Privacy',
  links: [
    {
      name: 'Privacy Policy',
      url: '#',
    },
    {
      name: 'Terms of use',
      url: '#',
    },
    {
      name: 'Security',
      url: '#',
    },
  ],
};

export const footerLinks = {
  main: { islands, games, nfts, tokens, socials },
  sub: {
    resources,
    supports,
    terms,
  },
};
