import dirTree from 'directory-tree';
import * as fs from 'fs';

const baseRoute = '/';
const routes = [baseRoute];
const date = new Date().toISOString().split('T')[0];

function getSitemapXML(domain, routes) {
  let sitemap = '<?xml version="1.0" encoding="UTF-8"?>\n';
  sitemap += '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n';
  routes.forEach((route) => {
    sitemap += getSitemapUrl(domain + route);
  });
  sitemap += '</urlset>';
  return sitemap;
}

function getSitemapUrl(location) {
  const url =
    ' <url>\n' + `  <loc>${location}</loc>\n` + `  <lastmod>${date}</lastmod>\n` + ' </url>\n';
  return url;
}

function getEndpoints(tree, route) {
  tree.children.forEach((child) => {
    if (child.children != undefined && child.children.length != 0) {
      let childRoute = route + child.name;
      if (child.children.some((e) => e.name === '+page.svelte')) {
        routes.push(childRoute);
      }
      getEndpoints(child, childRoute + '/');
    }
  });
}

const tree = dirTree('./src/routes');

getEndpoints(tree, baseRoute);

// Retrieve command-line arguments
const args = process.argv.slice(2);
const baseUrl = args[0] || 'https://example.com'; // Replace with your default base URL
const outputDir = args[1] || '.svelte-kit/output/client/'; // Replace with your default output directory

// YOUR_DOMAIN should be like https://example.com
const sitemap = getSitemapXML(baseUrl, routes);

// For vercel deployment use:
//fs.writeFileSync('.vercel/output/static/sitemap.xml', sitemap);
fs.writeFileSync(outputDir + '/sitemap.xml', sitemap);
